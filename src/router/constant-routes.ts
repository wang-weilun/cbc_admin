import type { RouteRecordRaw } from 'vue-router'

const Layout = () => import('@/layout/index.vue')

/** 常驻路由 */
export const constantRoutes: RouteRecordRaw[] = [
  {
    path: '/redirect',
    component: Layout,
    meta: { hidden: true },
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index.vue')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index.vue'),
    meta: { hidden: true }
  },

  {
    path: '/:pathMatch(.*)*',
    component: () => import('@/views/error/404.vue'),
    meta: { hidden: true }
  },
  {
    path: '/403',
    component: () => import('@/views/error/403.vue'),
    meta: { hidden: true }
  },

  {
    path: '',
    component: Layout,
    redirect: '/home',
    children: [
      {
        path: '/home',
        component: () => import('@/views/home/index.vue'),
        name: 'Home',
        meta: { title: '首页', icon: 'icon-dashboard', affix: true, svgIcon: 'menu-home' }
      }
    ]
  },
  // 设备管理的静态路由页面
  {
    path: '',
    component: Layout,
    redirect: '/equipment/newestData',
    meta: { hidden: true },
    children: [
      {
        path: '/equipment/newestData',
        component: () => import('@/views/equipment/newestData/index.vue'),
        name: 'newestData',
        meta: { title: '设备水质数据', icon: 'icon-dashboard', svgIcon: 'menu-home', hidden: true }
      }
    ]
  },
  {
    path: '',
    component: Layout,
    redirect: '/equipment/parameterDetails',
    meta: { hidden: true },
    children: [
      {
        path: '/equipment/parameterDetails',
        component: () => import('@/views/equipment/parameterDetails/index.vue'),
        name: 'parameterDetails',
        meta: { title: '参数详情', icon: 'icon-dashboard', svgIcon: 'menu-home', hidden: true }
      }
    ]
  },
  // 用户管理--用户详情信息页面
  {
    path: '',
    component: Layout,
    redirect: '/user/userDetail',
    meta: { hidden: true },
    children: [
      {
        path: '/user/userDetail',
        component: () => import('@/views/user/userDetail/index.vue'),
        name: 'userDetail',
        meta: { title: '用户详情信息', icon: 'icon-dashboard', svgIcon: 'menu-home', hidden: true }
      }
    ]
  },
  // 用户管理--用户日志信息页面
  {
    path: '',
    component: Layout,
    redirect: '/user/userLog',
    meta: { hidden: true },
    children: [
      {
        path: '/user/userLog',
        component: () => import('@/views/user/userLog/index.vue'),
        name: 'UserLog',
        meta: { title: '用户日志', icon: 'icon-dashboard', svgIcon: 'menu-home', hidden: true }
      }
    ]
  },

  //equipmentLog
  // 设备管理-设备日志
  {
    path: '',
    component: Layout,
    redirect: '/equipment/equipmentLog',
    meta: { hidden: true },
    children: [
      {
        path: '/equipment/equipmentLog',
        component: () => import('@/views/equipment/equipmentLog/index.vue'),
        name: 'devLog',
        meta: { title: '设备日志', icon: 'icon-dashboard', svgIcon: 'menu-home', hidden: true }
      }
    ]
  },

  // 设备管理--采集信息  gatherMessage
  {
    path: '',
    component: Layout,
    redirect: '/equipment/gatherMessage',
    meta: { hidden: true },
    children: [
      {
        path: '/equipment/gatherMessage',
        component: () => import('@/views/equipment/gatherMessage/index.vue'),
        name: 'gatherMessage',
        meta: { title: '采集信息', icon: 'icon-dashboard', svgIcon: 'menu-home', hidden: true }
      }
    ]
  },
  // 设备管理--传输配置  transferConf
  {
    path: '',
    component: Layout,
    redirect: '/equipment/transferConf',
    meta: { hidden: true },
    children: [
      {
        path: '/equipment/transferConf',
        component: () => import('@/views/equipment/transferConf/index.vue'),
        name: 'transferConf',
        meta: { title: '传输配置', icon: 'icon-dashboard', svgIcon: 'menu-home', hidden: true }
      }
    ]
  },
  // 设备管理--合格标准  criterionAcceptability
  {
    path: '',
    component: Layout,
    redirect: '/equipment/criterionAcceptability',
    meta: { hidden: true },
    children: [
      {
        path: '/equipment/criterionAcceptability',
        component: () => import('@/views/equipment/criterionAcceptability/index.vue'),
        name: 'criterionAcceptability',
        meta: { title: '合格标准', icon: 'icon-dashboard', svgIcon: 'menu-home', hidden: true }
      }
    ]
  },
  // 子用户管理--子用户关联设备 
  {
    path: '',
    component: Layout,
    redirect: '/subUser/dev',
    meta: { hidden: true },
    children: [
      {
        path: '/subUser/dev',
        component: () => import('@/views/subUserDevM/index.vue'),
        name: 'subUserDevM',
        meta: { title: '子用户关联设备管理', icon: 'icon-dashboard', svgIcon: 'menu-home', hidden: true }
      }
    ]
  },
]

export default constantRoutes
