import http from '@/utils/http'
import { prefix } from '../config'
import type * as User from './type'
// import type { RouteRecordRaw } from 'vue-router'

// 使用枚举包含所有的path
enum Api {
  // 登录
  login = 'mg/mLogin',
  // 获取当前用户路由列表
  getUserRoutesList = 'mg/platFormRoleModule/selectByUserId'
  // 获取天气数据
}

/** @desc 登录 */
export function login(platFormUserName: string, platFormUserPass: string) {
  // getUserRoutesList()
  return http.post<User.LoginRes>(
    `${ prefix }${ Api.login }?platFormUserName=${ platFormUserName }&platFormUserPass=${ platFormUserPass }`
  )
}

// 获取登录用户的路由列表
export const getUserRoutesList = () => {
  return http.post(`${ prefix }${ Api.getUserRoutesList }?platFormUserId=1`)
}

/** @desc 退出登录 */
export function logout() {
  return http.post(`${ prefix }/user/logout`)
}

/** @desc 获取用户信息 */
export const getUserInfo = () => {
  return http.post<User.UserInfo>(`${ prefix }/user/getUserInfo`)
}

// /** @desc 获取用户路由信息 */
// export const getUserRoutesList = () => {
//   return http.get<RouteRecordRaw[]>(`${prefix}/user/getUserRoutesList`)
// }
