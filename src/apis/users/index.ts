// 引入http
import request from '@/utils/http.ts'

// 使用枚举包含所有path
enum Api {
  // 查询用户列表
  getUsersList = 'mg/user/selectPage',
  // 查询用户日志列表
  getUserLogList = 'mg/userLog/selectByPage'
}

// 查询用户列表的Api
export const getUsersListApi = (data: any) => {
  return request({
    url: Api.getUsersList,
    method: 'post',
    params: data
  })
}

// 获取用户日志列表的Api
export const getUserLogListApi = (data: object) => {
  return request({
    url: Api.getUserLogList,
    method: 'post',
    params: data
  })
}
