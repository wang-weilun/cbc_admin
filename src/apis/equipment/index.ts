// 引入http
import request from '@/utils/http.ts'

// 使用枚举包含设备管理页面的所有地址
enum Api {
  // 获取设备分页列表
  getDevList = 'mg/dev/selectPage',
  // 获取查询的设备类型列表
  getDevTypeList = 'mg/DevType/selectList',
  // 获取当前设备的最新数据
  getCurrentDevNewestDataList = 'mg/checkData/selectLast',
  // 查询当前选择参数的历史记录
  getCurrentSelectParameterRecordList = 'mg/checkData/selectByPage',
  // 获取的导出Excel文件流
  getExprotExcelData = 'mg/checkData/exportCheckData',
  // 查询所有设备标识
  getAllDevFlagConf = 'mg/sysDictItem/selectDevFlagConf',
  // 查询当前设备标识
  getCurrentDevFlagConf = 'mg/devFlagConf/selectByDevById',
  // 设备添加标识
  addDevFlagConf = 'mg/devFlagConf/add',
  // 设备添删除标识
  delDevFlagConf = 'mg/devFlagConf/delById',
  // 解绑设备
  unbundleDev = 'mg/dev/disBind',
  // 删除设备
  delDev = 'mg/dev/delete',
  // 添加设备
  addDev = 'mg/dev/add',
  // 复制设备
  copyDev = 'mg/dev/copy',
  // 修改设备名称
  editDevName = 'mg/edit/devName',
  // 修改设备传输配置
  editDevConfig = 'mg/edit/devConConfig',
  // 查询单个设备的配置传输信息
  getDevConfig = 'mg/select/devConConfig',
  // 查询设备详情信息
  getDevDetail = 'mg/select/devDetailData',
  // 修改设备详情信息
  editDevDetail = 'mg/edit/deviceData',
  // 分页查询设备日志
  getDevLogList = 'mg/devLog/selectByPage',
  // 查询所有日志类型
  getAllDevLogTypeList = 'mg/sysDictItem/selectLogType',
  // 查询所有日志事件类型
  getAllDevLogEventTypeList = 'mg/sysDictItem/selectEventType',
  // 获取设备采集信息
  getDevGatherMessageList = 'mg/dev/collectSet',
  // 修改设备采集频率
  editDevGatherFrequency = 'mg/devCollectRate/edit',
  // 修改设备数据开关
  editDevDataSwitch = 'mg/devSwitch/edit',
  // 传输配置查询
  selectDevParamConf = 'mg/devConConfig/select',
  // 查询合格标准数据的Api
  selectDevStandRange = 'mg/devStandRange/selectByDevId',
  // 添加合格标准
  addDevStandRange = 'mg/devStandRange/add',
  // 修改合格标准
  editDevStandRange = 'mg/devStandRange/edit',
  // 启用合格标准
  openDevStandRange = 'mg/devStandRange/open',
  // 关闭合格标准
  closeDevStandRange = 'mg/devStandRange/close'
}

// 分页获取设备列表的Api
export const getDevListApi = (data: any) => {
  return request({
    url: Api.getDevList,
    method: 'post',
    params: data
  })
}

// 获取查询的设备类型列表的Api
export const getDevTypeListApi = () => {
  return request({
    url: Api.getDevTypeList,
    method: 'post'
  })
}

// 获取当前设备的最新数据的Api
export const getCurrentDevNewestDataListApi = (devId: number) => {
  return request({
    url: Api.getCurrentDevNewestDataList,
    method: 'get',
    params: {
      devId: devId
    }
  })
}

// 查询当前选择参数的历史记录的Api
export const getCurrentSelectParameterRecordListApi = (data: any) => {
  return request({
    url: Api.getCurrentSelectParameterRecordList,
    method: 'post',
    params: {
      devId: data.devId,
      paramName: data.paramName,
      page: data.page,
      size: data.size,
      startTime: data.startTime,
      endTime: data.endTime
    }
  })
}

// 获取的导出Excel文件流Api
export const getExprotExcelDataApi = (data: any) => {
  return request({
    url: Api.getExprotExcelData,
    method: 'post',
    params: {
      devId: data.devId,
      startTime: data.startTime,
      endTime: data.endTime
    },
    responseType: 'blob',
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 查询所有设备标识的Api
export const getAllDevFlagConfApi = () => {
  return request({
    url: Api.getAllDevFlagConf,
    method: 'post'
  })
}

// 查询当前设备标识的Api
export const getCurrentDevFlagConfApi = (devId: number) => {
  return request({
    url: Api.getCurrentDevFlagConf,
    method: 'post',
    params: {
      devId: devId
    }
  })
}

// 设备添加标识的Api
export const addDevFlagConfApi = (data: any) => {
  return request({
    url: Api.addDevFlagConf,
    method: 'post',
    params: {
      devId: data.devId,
      devFlagConfCode: data.devFlagConfCode
    }
  })
}

// 设备添删除标识的Api
export const delDevFlagConfApi = (devFlagConfId: any) => {
  return request({
    url: Api.delDevFlagConf,
    method: 'post',
    params: {
      devFlagConfId: devFlagConfId
    }
  })
}

// 解绑设备的Api
export const unbundleDevApi = (data: any) => {
  return request({
    url: Api.unbundleDev,
    method: 'post',
    params: {
      devId: data.devId,
      platFormUserId: data.platFormUserId
    }
  })
}

// 删除设备的Api
export const delDevApi = (data: any) => {
  return request({
    url: Api.delDev,
    method: 'post',
    params: {
      devId: data.devId,
      checkCode: data.checkCode
    }
  })
}

// 添加设备的Api
export const addDevApi = (data: any) => {
  return request({
    url: Api.addDev,
    method: 'post',
    params: {
      devGId: data.devGId,
      operateUser: data.operateUser,
      devDesc: data.devDesc
    }
  })
}

// 复制设备的Api
export const copyDevApi = (data: any) => {
  return request({
    url: Api.copyDev,
    method: 'post',
    params: {
      devGId: data.devGId,
      sourceDevGId: data.sourceDevGId,
      verifyCode: data.verifyCode
    }
  })
}

// 修改设备名称的Api
export const editDevNameApi = (data: any) => {
  return request({
    url: Api.editDevName,
    method: 'post',
    params: {
      devId: data.devId,
      newDevName: data.newDevName
    }
  })
}
// 修改设备传输配置的Api
export const editDevConfigApi = (data: any) => {
  return request({
    url: Api.editDevConfig,
    method: 'post',
    params: {
      devId: data.devId
    }
  })
}
// 查询单个设备的配置传输信息的Api
export const getDevConfigApi = (data: any) => {
  return request({
    url: Api.getDevConfig,
    method: 'post',
    params: {
      devId: data.devId
    }
  })
}
// 查询设备详情信息的Api
export const getDevDetailApi = (data: any) => {
  return request({
    url: Api.getDevDetail,
    method: 'post',
    params: {
      devId: data.devId
    }
  })
}
// 修改设备详情信息的Api
export const editDevDetailApi = (data: any) => {
  return request({
    url: Api.editDevDetail,
    method: 'post',
    params: {
      devId: data.devId
    }
  })
}

// 分页查询设备日志的Api
export const getDevLogListApi = (data: any) => {
  return request({
    url: Api.getDevLogList,
    method: 'post',
    params: data
  })
}

// 查询所有日志类型的Api
export const getAllDevLogTypeListApi = () => {
  return request({
    url: Api.getAllDevLogTypeList,
    method: 'post'
  })
}

// 查询所有日志事件类型的Api
export const getAllDevLogEventTypeListApi = () => {
  return request({
    url: Api.getAllDevLogEventTypeList,
    method: 'post'
  })
}

// 获取设备采集信息的Api
export const getDevGatherMessageListApi = (data: any) => {
  return request({
    url: Api.getDevGatherMessageList,
    method: 'post',
    params: data
  })
}

// 修改设备采集频率的Api
export const editDevGatherFrequencyApi = (data: any) => {
  return request({
    url: Api.editDevGatherFrequency,
    method: 'post',
    params: data
  })
}

// 修改设备数据开关的Api
export const editDevDataSwitchApi = (data: any) => {
  return request({
    url: Api.editDevDataSwitch,
    method: 'post',
    params: data
  })
}

// 传输配置查询的Api
export const selectDevParamConfApi = (data: any) => {
  return request({
    url: Api.selectDevParamConf,
    method: 'post',
    params: data
  })
}

// 查询合格标准数据的Api
export const selectDevStandRangeApi = (data: any) => {
  return request({
    url: Api.selectDevStandRange,
    method: 'post',
    params: data
  })
}

// 添加合格标准的Api
export const addDevStandRangeApi = (data: any) => {
  return request({
    url: Api.addDevStandRange,
    method: 'post',
    params: data
  })
}

// 修改合格标准的Api
export const editDevStandRangeApi = (data: any) => {
  return request({
    url: Api.editDevStandRange,
    method: 'post',
    params: data
  })
}

// 启用合格标准的Api
export const openDevStandRangeApi = (data: any) => {
  return request({
    url: Api.openDevStandRange,
    method: 'post',
    params: data
  })
}

// 关闭合格标准的Api
export const closeDevStandRangeApi = (data: any) => {
  return request({
    url: Api.closeDevStandRange,
    method: 'post',
    params: data
  })
}
