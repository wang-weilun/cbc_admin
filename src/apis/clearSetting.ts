import request from "@/utils/http";
import qs from 'qs'

// 使用枚举包含所有path
enum Api {
  // 清除日志
  clearLog = "mg/devLog/delete",
  // 查询日志总条数
  selectLogCount = 'mg/devLog/selectCount',
  // 查询报警记录总条数
  selectAlarmCount = 'mg/devReportData/selectCount',
  // 清除报警记录
  clearAlarm = 'mg/devReportData/delete'
}

// 清除日志的Api
/**
 * 清除日志接口的API调用。
 * @param data 包含结束时间的对象，用于指定要清除的日志的时间范围。
 * @returns 返回一个请求对象，通过这个请求可以清除指定时间范围内的日志。
 */
export function clearLogApi(data: { endTime: string }) {
  let dataQ = '?' + qs.stringify(data)
  return request.get(Api.clearLog + dataQ)
}

// 查询日志总条数的Api
export const selectLogCountApi = () => {
  return request.get(Api.selectLogCount)
}

// 查询报警记录总条数的Api
export const selectAlarmCountApi = () => {
  return request.get(Api.selectAlarmCount)
}

// 清除报警记录的Api
export const clearAlarmApi = (data: { endTime: string }) => {
  let dataQ = '?' + qs.stringify(data)
  return request.get(Api.clearAlarm + dataQ)
}