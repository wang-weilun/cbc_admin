// 引入http
import request from '@/utils/http.ts'

// 使用枚举包含所有地址
enum Api {
  // 获取数据字典分类列表
  getInformationTypeList = 'mg/sysDictType/select',
  // 查询字典数据列表
  getInformationList = 'mg/sysDictItem/selectByPage',
  // 添加字典数据
  addInformation = 'mg/sysDictItem/add',
  // 禁用字典数据
  disableInformation = 'mg/sysDictItem/disable',
  // 启用字典数据
  enableInformation = 'mg/sysDictItem/enable',
  // 修改字典数据
  editInformation = 'mg/sysDictItem/edit',
  // 添加字典类型
  addInformationType = 'mg/sysDictType/add'
}

// 获取数据字典分类列表的Api
export const getInformationTypeListApi = () => {
  return request({
    url: Api.getInformationTypeList,
    method: 'post'
  })
}

// 获取数据字典列表的Api
export const getInformationListApi = (data: any) => {
  return request({
    url: Api.getInformationList,
    method: 'post',
    params: data
  })
}

// 添加字典数据的Api
export const addInformationApi = (data: any) => {
  return request({
    url: Api.addInformation,
    method: 'post',
    params: data
  })
}

// 禁用字典数据的Api
export const disableInformationApi = (data: any) => {
  return request({
    url: Api.disableInformation,
    method: 'post',
    params: data
  })
}

// 启用字典数据的Api
export const enableInformationApi = (data: any) => {
  return request({
    url: Api.enableInformation,
    method: 'post',
    params: data
  })
}

// 修改字典数据的Api
export const editInformationApi = (data: any) => {
  return request({
    url: Api.editInformation,
    method: 'post',
    params: data
  })
}

// 添加字典类型的Api
export const addInformationTypeApi = (data: any) => {
  return request({
    url: Api.addInformationType,
    method: 'post',
    params: data
  })
}
