// 引入
import request from '@/utils/http.ts'
import qs from 'qs'


// 使用枚举包含所有的path
enum Api {
  // 查询子用户列表
  selectSubUserList = 'platFormSubUser/select/subUserlist',
  // 查询子用户下的设备
  selectDevListBySubUser = 'platFormSubUser/select/devListBySubUser',
  // 子用户绑定设备
  selectBindDevice = 'platFormSubUser/bind/device',
  // 获取设备分页列表
  getDevList = 'mg/dev/selectPage',
}




// 查询子用户列表的Api
export const selectSubUserListApi = () => {
  return request.get(Api.selectSubUserList)
}

// 查询子用户下的设备的Api
export const selectDevListBySubUserApi = (platFormSubUserId: number) => {
  return request.post(Api.selectDevListBySubUser + `?platFormSubUserId=${ platFormSubUserId }`)
}

// 子用户绑定设备的Api
export const bindDeviceApi = (data: { platFormSubUserId: number, devId: number }) => {
  return request({
    url: Api.selectBindDevice,
    method: 'post',
    params: data
  })
}


// 查询设备列表的Api
export const selectDevListApi = (data: any) => {
  return request({
    url: Api.getDevList,
    method: 'post',
    params: data
  })
}

