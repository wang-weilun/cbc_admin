import request from '@/utils/http'

// 使用枚举包含所有的path
enum Api {
  // 上传文件的信息
  addFile = 'mg/productFile/add',
  // 获取文件类型
  getFileTypeList = 'mg/sysDictItem/selectUploadType'
}

// 上传文件的信息的Api
export const addFileApi = (data: any) => {
  return request({
    url: Api.addFile,
    method: 'post',
    params: data
  })
}

// 获取文件类型的Api
export const getFileTypeListApi = () => {
  return request({
    url: Api.getFileTypeList,
    method: 'post'
  })
}
