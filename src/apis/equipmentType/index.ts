// 引入http
import request from '@/utils/http.ts'

// 使用枚举包含所有地址
enum Api {
  // 获取设备型号分页列表
  getEquipmentTypeList = 'mg/DevType/selectByPage'
}

// 获取设备型号分页列表的Api
export const getEquipmentTypeListApi = (data: any) => {
  return request({
    url: Api.getEquipmentTypeList,
    method: 'post',
    params: data
  })
}
