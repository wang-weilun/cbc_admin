export const exRoutes = [
  {
    "moduleId": 1,
    "moduleUrl": "/home",
    "moduleName": "首页",
    "moduleDesc": "",
    "moduleSort": "1",
    "moduleIcon": "",
    "sunPlatFormModuleList": []
  },
  {
    "moduleId": 2,
    "moduleUrl": "/equipment",
    "moduleName": "设备管理",
    "moduleDesc": "",
    "moduleSort": "2",
    "moduleIcon": "",
    "sunPlatFormModuleList": []
  },
  {
    "moduleId": 3,
    "moduleUrl": "/user",
    "moduleName": "用户管理",
    "moduleDesc": "",
    "moduleSort": "3",
    "moduleIcon": "",
    "sunPlatFormModuleList": []
  },
  {
    "moduleId": 4,
    "moduleUrl": "/subUser",
    "moduleName": "子用户管理",
    "moduleDesc": "",
    "moduleSort": "4",
    "moduleIcon": "",
    "sunPlatFormModuleList": []
  },
  {
    "moduleId": 5,
    "moduleUrl": "/equipmentType",
    "moduleName": "设备型号",
    "moduleDesc": "",
    "moduleSort": "5",
    "moduleIcon": "",
    "sunPlatFormModuleList": []
  },
  {
    "moduleId": 6,
    "moduleUrl": "/information",
    "moduleName": "数据字典",
    "moduleDesc": null,
    "moduleSort": "6",
    "moduleIcon": "",
    "sunPlatFormModuleList": []
  },
  {
    "moduleId": 7,
    "moduleUrl": "/authority",
    "moduleName": "权限管理",
    "moduleDesc": null,
    "moduleSort": "7",
    "moduleIcon": "",
    "sunPlatFormModuleList": [
      {
        "moduleId": 11,
        "moduleUrl": "authority/sysdict_items/index",
        "moduleName": "角色管理",
        "moduleDesc": "",
        "moduleSort": "1",
        "moduleIcon": ""
      },
      {
        "moduleId": 12,
        "moduleUrl": "authority/parameters/index",
        "moduleName": "界面配置",
        "moduleDesc": "",
        "moduleSort": "2",
        "moduleIcon": ""
      },
      {
        "moduleId": 13,
        "moduleUrl": "authority/roleAuthority/index",
        "moduleName": "角色权限",
        "moduleDesc": "",
        "moduleSort": "3",
        "moduleIcon": ""
      },
      {
        "moduleId": 14,
        "moduleUrl": "authority/rolePage/index",
        "moduleName": "角色界面",
        "moduleDesc": "",
        "moduleSort": "4",
        "moduleIcon": ""
      },
      {
        "moduleId": 15,
        "moduleUrl": "authority/administrator/index",
        "moduleName": "管理配置",
        "moduleDesc": "",
        "moduleSort": "5",
        "moduleIcon": ""
      }
    ]
  },
  {
    "moduleId": 8,
    "moduleUrl": "/files",
    "moduleName": "文件管理",
    "moduleDesc": "通过设备列表进入，侧边栏隐藏",
    "moduleSort": "8",
    "moduleIcon": "",
    "sunPlatFormModuleList": []
  },
  {
    "moduleId": 9,
    "moduleUrl": "/personageSet",
    "moduleName": "个人设置",
    "moduleDesc": "通过历史数据进入，侧边栏隐藏",
    "moduleSort": "9",
    "moduleIcon": "",
    "sunPlatFormModuleList": []
  },
  // {
  //   "moduleId": 10,
  //   "moduleUrl": "/systemSetting",
  //   "moduleName": "系统设置",
  //   "moduleDesc": "对系统进行管理设置",
  //   "moduleSort": "10",
  //   "moduleIcon": "",
  //   "sunPlatFormModuleList": []
  // }
] 