import { defineStore } from 'pinia'
import { ref, computed } from 'vue'
import { getUsersListApi } from '@/apis/users/index.ts'
import { Message } from '@arco-design/web-vue'

const storeSetup = () => {
  /* 定义数据--------------------------------------------------------------------- */
  // 请求用户数据列表的参数对象数据
  const getListInfo = ref({
    userName: '',
    userPhone: '',
    userCompany: '',
    page: 1,
    size: 10
  })
  // 用户的userId
  const userInfo = ref({})

  // 总条数
  const total = ref(0)

  // 存储用户列表数据的数据
  const usersList = ref([])

  // 用户信息列表--表格的加载状态标识
  const isUserTableLoading = ref(false)

  /* 定义函数--------------------------------------------------------------------- */
  // 获取用户数据列表的函数
  const getUsersListFunc = async () => {
    isUserTableLoading.value = true
    const res = await getUsersListApi(getListInfo.value)
    // console.log(res)
    usersList.value = res.data.list
    total.value = res.data.total
    console.log(usersList.value)
    isUserTableLoading.value = false
  }

  // 重置头部查询表单的数据
  const resetHeaderForm = () => {
    getListInfo.value.userName = ''
    getListInfo.value.userPhone = ''
    getListInfo.value.userCompany = ''
    getListInfo.value.page = 1
    getUsersListFunc()
      .then(() => {
        Message.success('重置成功')
      })
      .catch(() => {
        Message.success('重置失败')
      })
  }
  // 返回参数
  return {
    getUsersListFunc,
    getListInfo,
    usersList,
    total,
    isUserTableLoading,
    resetHeaderForm,
    userInfo
  }
}

// 导出Store
export const useUsersStore = defineStore('users', storeSetup, { persist: true })
