import { defineStore } from 'pinia'
import { ref, computed } from 'vue'
import {
  getDevListApi,
  getCurrentDevNewestDataListApi,
  getAllDevFlagConfApi,
  getDevLogListApi,
  getAllDevLogTypeListApi,
  getAllDevLogEventTypeListApi,
  selectDevParamConfApi
} from '@/apis/equipment/index.ts'
import { Message } from '@arco-design/web-vue'

const storeSetup = () => {
  /* 定义数据--------------------------------------------------------------------- */

  // 当前页
  const currentPge = ref(1)
  // 每页条数
  const pageSize = ref(10)
  // 总条数
  const total = ref(0)
  // 存储设备列表的数据
  const devList = ref([])
  // 表格加载的标识
  const isLoading = ref(true)
  // 表格查询的信息
  const searchForm = ref({
    // 设备编码
    devStatusCode: '',
    // 设备名称
    devName: '',
    // 设备类型
    devTypeId: '',
    // 用户手机号
    userPhone: '',
    // 状态
    devStatusName: '',
    // 行业
    devGId: '',
    // 设备标识
    devFlagConfCode: ''
  })

  // 设备管理页面传递给设备水质数据参数列表页面的参数
  const devInfo = ref({
    devId: '',
    devName: ''
  })

  // 参数列表传递给参数详情历史记录页面的参数
  const paramInfo = ref({
    paramName: '',
    paramValueUnit: ''
  })

  // 每个设备的设备水质数据列表
  const devDataList = ref([])

  // 存放设备参数列表的数据
  const paramList = ref([])

  // 存放设备标识列表的数据
  const devFlagConfList = ref([])

  // 控制展示设备的数据弹出框的加载状态标识
  const isShowDevDataModalLoading = ref(false)

  // 传输配置---是否是JSON格式的传输协议
  const isJSONParamConfList = computed(() => {
    return confInfoData.value.devDataTypeName == 'JSON'
  })

  // 是否参数集已经双击数据
  const isConfListDbClick = ref(false)

  // 设备传输配置---配置信息数据
  const confInfoData = ref({
    devTxTypeName: '',
    devDataTypeName: '', // JSON--0019 / MODBUS--0018
    devHeart: 'METER' // 心跳包
  })

  // 传输配置---传输配置的参数集数据
  const paramsConfList = ref([])

  // 传输配置---参数配置数据
  const parmasConfDataList = ref([])

  // 当前双击项的功能码和485地址
  const currentFunCodeAnd485AddressData = ref({
    funCode: '',
    address485: ''
  })


  /* 定义函数--------------------------------------------------------------------- */
  // 获取表格数据的函数
  // 获取设备列表的函数

  // 查询传输配置数据的函数
  const selectDevParamConfFunc = async () => {
    const res = await selectDevParamConfApi({ devId: devInfo.value.devId })
    console.log(res.data)
    confInfoData.value.devDataTypeName = res.data.devDataTypeName
    confInfoData.value.devHeart = res.data.devHeart
    confInfoData.value.devTxTypeName = res.data.devTxTypeName

    // 判断是JSON/MODBUS
    if (isJSONParamConfList.value) {
      paramsConfList.value = []
      parmasConfDataList.value = res.data.paramConfList.paramConfDataList
    } else {
      paramsConfList.value = res.data.paramConfList
    }
  }

  // 参数集列表每一项的点击事件函数
  const paramsConfListDbclick = (e: any) => {
    console.log(e)
    currentFunCodeAnd485AddressData.value.funCode = e.funCode
    currentFunCodeAnd485AddressData.value.address485 = e.addressCode
    parmasConfDataList.value = e.paramConfDataList
    isConfListDbClick.value = true
  }

  const getDevListFunc = async () => {
    isLoading.value = true
    console.log(currentPge.value, pageSize.value)
    const res = await getDevListApi({
      page: currentPge.value,
      size: pageSize.value,
      devName: searchForm.value.devName,
      userPhone: searchForm.value.userPhone,
      devStatusCode: searchForm.value.devStatusName,
      devTypeId: searchForm.value.devTypeId,
      devGId: searchForm.value.devGId,
      devFlagConfCode: searchForm.value.devFlagConfCode
    })
    devList.value = res.data.list
    total.value = res.data.total
    isLoading.value = false
    console.log(devList.value)
    // Message.success('加载成功')
  }

  // 获取设备水质数据的函数
  const getCurrentDevNewestDataListFunc = async (devId: number) => {
    isShowDevDataModalLoading.value = true
    const res = await getCurrentDevNewestDataListApi(devId)
    console.log(res)
    devDataList.value = res.data
    isShowDevDataModalLoading.value = false
  }

  // 清空头部表单内容的函数
  const resetForm = () => {
    Object.keys(searchForm.value).forEach((key) => {
      searchForm.value[key] = ''
    })
    getDevListFunc()
  }

  // 获取标识列表的函数
  const getFlagConfListFunc = async () => {
    const res = await getAllDevFlagConfApi()
    // console.log(res)
    devFlagConfList.value = res.data
  }

  // 返回参数
  return {
    searchForm,
    currentPge,
    pageSize,
    total,
    devList,
    devDataList,
    getDevListFunc,
    getCurrentDevNewestDataListFunc,
    isLoading,
    resetForm,
    devInfo,
    paramList,
    paramInfo,
    isShowDevDataModalLoading,
    getFlagConfListFunc,
    devFlagConfList,
    confInfoData,
    isJSONParamConfList,
    paramsConfList,
    parmasConfDataList,
    selectDevParamConfFunc,
    paramsConfListDbclick,
    isConfListDbClick,
    currentFunCodeAnd485AddressData
  }
}

// 导出Store
export const useEquipmentStore = defineStore('equipment', storeSetup, { persist: true })
