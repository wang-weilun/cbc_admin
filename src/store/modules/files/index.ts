import { defineStore } from 'pinia'
import { ref, computed } from 'vue'
import { Message } from '@arco-design/web-vue'
import { addFileApi, getFileTypeListApi } from '@/apis/file'

const storeSetup = () => {
  /* 定义数据--------------------------------------------------------------------- */
  // 面包屑数据数组
  // const breadcrumbData = ref([
  //   {
  //     name: '路径1'
  //   },
  //   {
  //     name: '路径2'
  //   },
  //   {
  //     name: '路径3'
  //   }
  // ])

  // 上传文件的地址
  const fileActionUrl = ref('http://192.168.0.161:11005/mg/upload/softImage')

  // 添加文件信息请求所需的参数
  const addFileParams = ref({
    fileName: '',
    fileUrl: '',
    filePath: '',
    fileTypeCode: '',
    uploadUser: JSON.parse(localStorage.getItem('userInfo')).platFormUserId,
    fileDesc: ''
  })

  // 当前accessToken
  const currentAccessToken = ref('')

  // 文件类型列表的数据
  const fileTypeList = ref([])

  // 显示上传文件弹窗的标识
  const isShowUploadModal = ref(false)

  /* 定义函数--------------------------------------------------------------------- */
  // 获取文件类型列表的函数
  const getFileTypeListFunc = async () => {
    const res = await getFileTypeListApi()
    fileTypeList.value = res.data
  }

  // 获取当前accessToken的函数
  const getAccessTOkenFunc = () => {
    currentAccessToken.value = localStorage.getItem('accessToken')
  }

  // 上传文件的函数
  const addFileFunc = async () => {
    await addFileApi(addFileParams.value)
      .then((res) => {
        if (res.result == 'error') {
          Message.error(res.msg)
        } else {
          Message.success('上传成功')
          isShowUploadModal.value = false
          addFileParams.value.fileDesc = ''
          addFileParams.value.fileName = ''
          addFileParams.value.filePath = ''
          addFileParams.value.fileUrl = ''
          addFileParams.value.fileTypeCode = ''
        }
      })
      .catch(() => {
        Message.error('上传失败')
      })
  }

  // 返回参数
  return {
    fileActionUrl,
    addFileParams,
    getFileTypeListFunc,
    fileTypeList,
    currentAccessToken,
    getAccessTOkenFunc,
    addFileApi,
    addFileFunc,
    isShowUploadModal
  }
}

// 导出Store
export const useFilesStore = defineStore('files', storeSetup, { persist: false })
