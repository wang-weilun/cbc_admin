import { defineStore } from 'pinia'
import { ref, computed } from 'vue'
import { Message } from '@arco-design/web-vue'
import { getEquipmentTypeListApi } from '@/apis/equipmentType/index'

const storeSetup = () => {
  /* 定义数据--------------------------------------------------------------------- */

  // 存储设备型号列表的数据
  const equipmentTypeList = ref([])

  // 获取设备型号列表数据的携带的参数
  const getEquipmentTypeListParam = ref({
    page: 1,
    size: 10,
    devTypeName: ''
  })

  // 表格数据的总条数
  const total = ref(0)
  /* 定义函数--------------------------------------------------------------------- */
  // 获取设备型号列表数据的函数
  const getEquipmentTypeListFunc = async () => {
    const res = await getEquipmentTypeListApi(getEquipmentTypeListParam.value)
    console.log(res)
    equipmentTypeList.value = res.data.list
    total.value = res.data.total
  }

  // 查询按钮的点击事件
  const searchClick = () => {
    getEquipmentTypeListParam.value.page = 1
    getEquipmentTypeListFunc()
      .then(() => {
        Message.success('查询成功')
      })
      .catch(() => {
        Message.success('查询失败')
      })
  }

  // 重置查询的点击事件
  const resetClick = () => {
    getEquipmentTypeListParam.value.page = 1
    getEquipmentTypeListParam.value.devTypeName = ''
    getEquipmentTypeListFunc()
      .then(() => {
        Message.success('重置成功')
      })
      .catch(() => {
        Message.success('重置失败')
      })
  }

  // 返回参数
  return {
    getEquipmentTypeListFunc,
    equipmentTypeList,
    getEquipmentTypeListParam,
    total,
    searchClick,
    resetClick
  }
}

// 导出Store
export const useEquipmentTypeStore = defineStore('equipmentType', storeSetup, { persist: false })
