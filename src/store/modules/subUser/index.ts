import { defineStore } from 'pinia'
import { ref, computed } from 'vue'
import { Message } from '@arco-design/web-vue'
import { selectSubUserListApi } from '@/apis/subUser'
import type { subUserListType, subUserType } from '@/views/subUser/type.ts'

const storeSetup = () => {
  /* 定义数据--------------------------------------------------------------------- */
  // 平台子用户数据
  const subUserList = ref<subUserListType>([])

  // 平台子用户数据表格加载状态
  const isSubUserTableLoading = ref(false)

  // 表单查询数据
  const formSearchData = ref<subUserType>({
    // 子用户用户名
    platFormSubUserName: '',
    // 子用户公司
    platFormSubUserCompany: '',
    // 子用户邮箱
    platFormSubUserEmail: '',
    // 子用户创建时间
    platFormSubUserCreateTime: '',
    // 子用户最近登录时间
    platFormSubUserLastLogin: '',
    // 子用户微信ID
    platFormSubUserWeichatId: null,
    // 子用户代码
    platFormSubUserCode: '',
    // 子用户备注
    platFormSubUserDesc: '',
    // 所在城市
    mainArea: ''
  })

  // 当前点击关联设备的用户信息
  const currentSubUserInfo = ref<subUserType>({
    platFormSubUserId: null,
    platFormSubUserName: '',
    platFormSubUserCompany: '',
    platFormSubUserEmail: '',
    platFormSubUserCreateTime: '',
    platFormSubUserLastLogin: '',
    platFormSubUserWeichatId: null,
    platFormSubUserCode: '',
    platFormSubUserDesc: '',
    mainArea: ''
  })


  /* 定义函数--------------------------------------------------------------------- */
  // 获取子用户列表的函数
  const getSubUserListFunc = async () => {
    isSubUserTableLoading.value = true
    await selectSubUserListApi().then((res) => {
      console.log(res)
      subUserList.value = res.data
      isSubUserTableLoading.value = false
    })
  }

  // 返回参数
  return {
    isSubUserTableLoading,
    subUserList,
    formSearchData,
    currentSubUserInfo,
    getSubUserListFunc
  }
}

// 导出Store
export const useSubUserStore = defineStore('SubUser', storeSetup, { persist: true })
