import { defineStore } from 'pinia'
import { ref, computed } from 'vue'
import { Message } from '@arco-design/web-vue'
import {
  getInformationTypeListApi,
  getInformationListApi,
  addInformationTypeApi,
  addInformationApi
} from '@/apis/information/index'
import { date2String } from '@/utils/myUtils'

const storeSetup = () => {
  /* 定义数据--------------------------------------------------------------------- */
  // 存储字典分类列表的数据
  const typeList = ref([])
  // 字典分类列表要显示的数据
  const typeShowList = ref([])
  // 存储字典数据列表的数据
  const informationList = ref([])
  // 字典分类搜索的关键字
  const typeListSearchKeyword = ref('')
  // 字典数据搜索的关键字
  const informationSearchKeywrod = ref('')
  // 字典数据搜索的字典编码关键字
  const informationSearchCodeKeywrod = ref('')
  // 存储当前选中的字典数据分类的数据
  const currentTypeActiveRow = ref({
    createTime: '',
    dictDesc: '',
    dictTypeCode: '',
    dictTypeName: '',
    id: 0,
    sort: 0,
    status: 0
  })
  // 字典数据表格加载的标识
  const isInformationTableLoading = ref(false)

  // 字典数据表格的总条数
  const total = ref(0)

  // 字典数据表格的当前页
  const current = ref(1)

  // 字典数据表格的当前页最大条数
  const size = ref(10)

  // 字典数据添加的弹窗显示的标识
  const isShowAddInformation = ref(false)

  // 字典数据编辑的弹窗显示的标识
  const isShowEditInformation = ref(false)

  // 是否显示字典类型卡片的标识
  const isShowInformationTypeCard = ref(false)

  // 添加字典类型的表单数据
  const addInformationTypeForm = ref({
    dictTypeName: '',
    dictDesc: ''
  })

  // 显示添加字典类型的弹窗的标识
  const isShowAddInformationTypeModal = ref(false)
  /* 定义函数--------------------------------------------------------------------- */
  // 请求分类列表的函数
  const getTypeListFunc = async () => {
    const res = await getInformationTypeListApi()
    typeList.value = res.data
    typeShowList.value = res.data
  }

  // 请求字典数据列表的函数
  const getInformationListFunc = async () => {
    isInformationTableLoading.value = true
    const res = await getInformationListApi({
      dictTypeCode: currentTypeActiveRow.value.dictTypeCode,
      dictCode: informationSearchCodeKeywrod.value,
      dictName: informationSearchKeywrod.value,
      status: null,
      page: current.value,
      size: size.value
    })
    // console.log(res)
    informationList.value = res.data.list
    res.data.list.forEach((item: any) => {
      item.isEdit = false
      item.createTime = date2String('YYYY-mm-dd HH:MM:SS', item.createTime)
    })
    total.value = res.data.total
    isInformationTableLoading.value = false
  }

  // 分类每一项的点击事件
  const typeItemClick = (row: object) => {
    current.value = 1
    size.value = 10
    console.log(row)

    currentTypeActiveRow.value = row
    getInformationListFunc()
      .then(() => {
        // Message.success('加载字典数据成功')
      })
      .catch(() => {
        // Message.success('加载字典数据失败')
      })
  }

  // 分类输入框的输入change事件
  const typeSearchInput = () => {
    let newArr = []
    typeList.value.forEach((item) => {
      if (item.dictTypeName.indexOf(typeListSearchKeyword.value) != -1) {
        newArr.push(item)
      }
    })
    typeShowList.value = newArr
  }

  // 字典数据类型的搜索框的清除按钮点击事件
  const typeSearchInputClear = () => {
    typeSearchInput()
  }
  // 字典数据类型的重置点击事件
  const resetTypeClick = () => {
    typeListSearchKeyword.value = ''
    getTypeListFunc()
      .then(() => {
        Message.success('刷新字典类型成功')
      })
      .catch(() => {
        Message.success('刷新字典类型失败')
      })
  }

  // 字典数据内容搜索条件的重置点击事件
  const informationTableResetClick = () => {
    currentTypeActiveRow.value.dictTypeCode = ''
    informationSearchCodeKeywrod.value = ''
    informationSearchKeywrod.value = ''
    current.value = 1
    size.value = 10
    getInformationListFunc()
  }

  // 字典数据表格当前页变化的change事件
  const currentPageChange = (e: any) => {
    current.value = e
    getInformationListFunc()
  }

  // 字典数据的查询按钮的点击事件
  const informationTableSearchClick = () => {
    current.value = 1
    size.value = 10
    getInformationListFunc()
      .then(() => {
        Message.success('查询成功')
      })
      .catch(() => {
        Message.success('查询失败')
      })
  }

  // 添加字典类型的点击事件
  const addInformationTypeClick = () => {
    isShowAddInformationTypeModal.value = true
    console.log('添加字典类型')
  }

  // 添加字典类型的请求函数
  const addInformationFunc = async () => {
    // isShowAddInformationTypeModal.value = false
    await addInformationTypeApi(addInformationTypeForm.value)
      .then(() => {
        Message.success('添加成功')
      })
      .catch(() => {
        Message.success('添加失败')
      })
    getTypeListFunc()
  }
  // 返回参数
  return {
    getTypeListFunc,
    typeItemClick,
    typeList,
    currentTypeActiveRow,
    typeSearchInput,
    typeShowList,
    resetTypeClick,
    typeListSearchKeyword,
    typeSearchInputClear,
    informationSearchKeywrod,
    informationSearchCodeKeywrod,
    informationList,
    total,
    isInformationTableLoading,
    current,
    size,
    currentPageChange,
    informationTableSearchClick,
    isShowAddInformation,
    isShowInformationTypeCard,
    addInformationTypeClick,
    isShowEditInformation,
    getInformationListFunc,
    addInformationFunc,
    addInformationTypeForm,
    isShowAddInformationTypeModal,
    informationTableResetClick
  }
}

// 导出Store
export const useInformationStore = defineStore('information', storeSetup, { persist: false })
