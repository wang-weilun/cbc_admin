import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

export * from './modules/app'
export * from './modules/permission'
export * from './modules/tabs'
export * from './modules/user'
export * from './modules/file'
export * from './modules/equipment'
export * from './modules/users'
export * from './modules/files'
export * from './modules/equipmentType'
export * from './modules/information'
export * from './modules/subUser'

const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)

export default pinia
