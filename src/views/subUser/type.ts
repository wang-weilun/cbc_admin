// 平台子用户类型
export interface subUserType {
  // 子用户ID
  platFormSubUserId?: number,
  // 子用户用户名
  platFormSubUserName: string,
  // 子用户公司
  platFormSubUserCompany: string,
  // 子用户邮箱
  platFormSubUserEmail: string,
  // 子用户创建时间
  platFormSubUserCreateTime: string,
  // 子用户最近登录时间
  platFormSubUserLastLogin: string,
  // 子用户微信ID
  platFormSubUserWeichatId?: number,
  // 子用户代码
  platFormSubUserCode: string,
  // 子用户备注
  platFormSubUserDesc: string,
  // 所在城市
  mainArea: string
}


// 平台子用户列表类型
export type subUserListType = subUserType[]




// 平台子用户关联设备类型
export interface subUserRelevanceDevType {
  devId: number,
  devName: string,
  devTypeId?: number,
  devGId: string,
  dtuId: string,
  userId?: number,
  devStatusCode: string,
  devCreateTime: string,
  totalOnlineTime: string,
  totalOfflineTime: string,
  lastOnlineTime: string,
  lastOfflineTime: string,
  isBind: number,
  operateUser?: number,
  devDesc: string,
  industryCode: string
}


// 平台子用户关联设备列表类型
export type subUserRelevanceDevListType = subUserRelevanceDevType[]