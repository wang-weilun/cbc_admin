// 格式化日期时间格式的函数
export const date2String = (fmt: any, dateString: any) => {
  const date = new Date(dateString)
  let ret
  const opt = {
    'Y+': date.getFullYear().toString(), // 年
    'm+': (date.getMonth() + 1).toString(), // 月
    'd+': date.getDate().toString(), // 日
    'H+': date.getHours().toString(), // 时
    'M+': date.getMinutes().toString(), // 分
    'S+': date.getSeconds().toString() // 秒
    // 有其他格式化字符需求可以继续添加，必须转化成字符串
  }
  for (let k in opt) {
    ret = new RegExp('(' + k + ')').exec(fmt)
    if (ret) {
      fmt = fmt.replace(ret[1], ret[1].length == 1 ? opt[k] : opt[k].padStart(ret[1].length, '0'))
    }
  }
  return fmt
}

// 获取当前时间的函数
export const getNowDate = () => {
  var date = new Date()
  var sign2 = ':'
  var year = date.getFullYear() // 年
  var month = date.getMonth() + 1 // 月
  var day = date.getDate() // 日
  var hour = date.getHours() // 时
  var minutes = date.getMinutes() // 分
  var seconds = date.getSeconds() //秒
  var weekArr = ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期天']
  var week = weekArr[date.getDay()]
  // 给一位数的数据前面加 “0”
  if (month >= 1 && month <= 9) {
    month = '0' + month
  }
  if (day >= 0 && day <= 9) {
    day = '0' + day
  }
  if (hour >= 0 && hour <= 9) {
    hour = '0' + hour
  }
  if (minutes >= 0 && minutes <= 9) {
    minutes = '0' + minutes
  }
  if (seconds >= 0 && seconds <= 9) {
    seconds = '0' + seconds
  }
  return year + '-' + month + '-' + day + ' ' + hour + sign2 + minutes + sign2 + seconds
}

// 将日期时间拆分成数组的函数
export const getRemainderTime = (startTime: any) => {
  var s1 = new Date(startTime.replace(/-/g, '/')),
    s2 = new Date(),
    runTime = parseInt((s2.getTime() - s1.getTime()) / 1000)
  var year = Math.floor(runTime / 86400 / 365)
  runTime = runTime % (86400 * 365)
  var month = Math.floor(runTime / 86400 / 30)
  runTime = runTime % (86400 * 30)
  var day = Math.floor(runTime / 86400)
  runTime = runTime % 86400
  var hour = Math.floor(runTime / 3600)
  runTime = runTime % 3600
  var minute = Math.floor(runTime / 60)
  runTime = runTime % 60
  var second = runTime
  // console.log(year,month,day,hour,minute,second);
  // return year + ',' + month + ',' + day + ',' + hour + ',' + minute + ',' + second
  return [year, month, day, hour, minute, second]
}
