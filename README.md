# 恒蓝云后台管理系统

## 技术选型

> Vite + Vue3 + Arco Design + Axios



## 准备

- [Node](https://nodejs.p2hp.com/) 和 [Git](https://git-scm.com/) 项目开发环境
- [Vite](https://vitejs.cn/) - 熟悉 Vite 特性
- [TypeScript](https://www.typescriptlang.org/) - 熟悉 TypeScript 基本语法
- [Es6+](http://es6.ruanyifeng.com/) - 熟悉 ES6 基本语法
- [Vue-Router](https://router.vuejs.org/) - 熟悉 vue-router 基本使用

- [Arco Design](https://www.naiveui.com/) - UI组件库 基本使用



## 使用

- 安装依赖

  `npm install `

- 运行项目

  `npm run dev`

- 打包项目

  `npm run build`



## Git 贡献提交规范

- 参考[vue](https://github.com/vuejs/vue/blob/dev/.github/COMMIT_CONVENTION.md) 规范
  - `feat`增加新功能
  - `fix`修复问题/BUG
  - `style`代码风格相关无影响运行结果的
  - `perf`优化/性能提升
  - `refactor`重构
  - `revert`撤销修改
  - `test`测试相关
  - `docs`文档/注释
  - `chore`依赖更新/脚手架配置修改等
  - `workflow`工作流改进
  - `ci`持续集成
  - `types`类型定义文件更改
  - `wip`开发中



## 浏览器支持

本地开发推荐使用`Chrome 80+`浏览器

支持现代浏览器, 不支持 IE

| [![ Edge](https://img2.baidu.com/it/u=3804757358,2350388875&fm=253&fmt=auto&app=138&f=JPEG?w=758&h=500)](http://godban.github.io/browsers-support-badges/) IE | [![ Edge](https://img0.baidu.com/it/u=3381011594,3848495978&fm=253&fmt=auto&app=138&f=JPEG?w=800&h=500)](http://godban.github.io/browsers-support-badges/) Edge | [![Firefox](https://img2.baidu.com/it/u=3042486489,942411676&fm=253&fmt=auto&app=138&f=JPEG?w=693&h=500)](http://godban.github.io/browsers-support-badges/) Firefox | [![Chrome](https://img0.baidu.com/it/u=2377458196,2933869355&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500)](http://godban.github.io/browsers-support-badges/) Chrome | [![Safari](https://img1.baidu.com/it/u=436462708,2188630854&fm=253&fmt=auto&app=120&f=JPEG?w=500&h=361)](http://godban.github.io/browsers-support-badges/) Safari |
| :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| not support                                                  | last 2 versions                                              | last 2 versions                                              | last 2 versions                                              | last 2 versions                                              |





## 维护者

[@wang-weilun](https://gitee.com/wang-weilun)